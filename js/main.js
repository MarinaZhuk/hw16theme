const changeTheme = document.createElement("button");

changeTheme.classList.add("btn");
changeTheme.innerText = "Сменить тему страницы";
changeTheme.style.backgroundColor="orange";
changeTheme.style.marginTop="20px";

document.getElementsByClassName("title")[0].appendChild(changeTheme);

checkThemeStatus = () => {
    let status = localStorage.getItem("themeState");
    localStorage.setItem(status,'true');
    const link = document.createElement("link");
    document.head.appendChild(link);  
    link.rel = "stylesheet";
    if(status==='true'){
        link.href="css/theme.css";
    } else{
        link.href="css/main.css";
    }
};

checkThemeStatus();

changeTheme.addEventListener("click",()=>{
    let status = localStorage.getItem("themeState");
    localStorage.setItem(status,'true');
    if(status==='true'){
        localStorage.setItem("themeState","false");
    }else{
        localStorage.setItem("themeState","true");
    };
    checkThemeStatus();
});
